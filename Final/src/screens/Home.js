import React from 'react';
import { Row, Col } from 'antd';
import { Card } from 'antd';

const { Meta } = Card;
export default function Home() {
	const [data, setData] = React.useState([]);
	React.useEffect(() => {
		fetch('https://backendexample.sanbersy.com/api/data-movie')
			.then((response) => response.json())
			.then((data) => {
				setData(data);
			});
	}, []);
	return (
		<>
			<Row gutter={16}>
				{data.map((item) => (
					<Col span={4}>
						<Card hoverable cover={<img alt="example" src={item.image_url} />}>
							<Meta title={item.title} />
						</Card>
					</Col>
				))}
			</Row>
		</>
	);
}
