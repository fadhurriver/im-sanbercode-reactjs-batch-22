import React from 'react';
import { Row, Col, Table, Space, Button, Form, Input, Modal } from 'antd';
import { Card } from 'antd';
import axios from 'axios';
import { UserContext, UserProvider } from '../UserContext';
const { Meta } = Card;

export default function Home() {
	const [data, setData] = React.useState([]);
	const [user, setUser] = React.useContext(UserContext);
	const [selectedData, setSelectedData] = React.useState(false);
	const [input, setInput] = React.useState({
		title: '',
		image_url: '',
		genre: '',
		year: '',
		description: '',
		duration: '',
		rating: '',
		review: '',
	});
	const [isModalVisible, setIsModalVisible] = React.useState(false);
	const [isModalVisibleAdd, setIsModalVisibleAdd] = React.useState(false);

	React.useEffect(() => {
		fetch('https://backendexample.sanbersy.com/api/data-movie')
			.then((response) => response.json())
			.then((data) => setData(data));
	}, []);
	React.useEffect(() => {
		setSelectedData(false);
	}, [isModalVisible]);

	const handleChange = (event) => {
		let value = event.target.value;
		let name = event.target.name;
		switch (name) {
			case 'title': {
				setInput({ ...input, title: value });
				break;
			}
			case 'image_url': {
				setInput({ ...input, image_url: value });
				break;
			}
			case 'genre': {
				setInput({ ...input, genre: value });
				break;
			}
			case 'year': {
				setInput({ ...input, year: value });
				break;
			}

			case 'description': {
				setInput({ ...input, description: value });
				break;
			}
			case 'duration': {
				setInput({ ...input, duration: value });
				break;
			}
			case 'rating': {
				setInput({ ...input, rating: value });
				break;
			}
			case 'review': {
				setInput({ ...input, review: value });
				break;
			}

			default: {
				break;
			}
		}
	};

	// Edit
	const showModal = (item) => {
		setIsModalVisible(true);
		setSelectedData(item);
	};

	const handleOk = () => {
		setIsModalVisible(false);
		setSelectedData(false);
	};

	const onSubmit = () => {
		axios
			.put('https://backendexample.sanbersy.com/api/data-movie', input, {
				headers: { Authorization: 'Bearer ' + user.token },
			})
			.then((res) => {
				console.log(res);
			})
			.catch((e) => alert(e));
	};
	const handleCancel = () => {
		setIsModalVisible(false);
		setSelectedData(false);
	};
	// Tambah
	const showModalAdd = (item) => {
		setIsModalVisibleAdd(true);
	};

	const handleOkAdd = () => {
		setIsModalVisible(false);
	};

	const onSubmitAdd = () => {
		axios
			.post('https://backendexample.sanbersy.com/api/data-movie', input, {
				headers: { Authorization: 'Bearer ' + user.token },
			})
			.then((res) => {
				console.log(res);
			})
			.catch((e) => alert(e));
	};
	const handleCancelAdd = () => {
		setIsModalVisibleAdd(false);
	};

	const layout = {
		labelCol: { span: 8 },
		wrapperCol: { span: 16 },
	};
	const tailLayout = {
		wrapperCol: { offset: 8, span: 16 },
	};

	const columns = [
		{
			title: 'ID',
			dataIndex: 'id',
			key: 'id',
		},
		{
			title: 'Title',
			dataIndex: 'title',
			key: 'title',
		},
		{
			title: 'Genre',
			dataIndex: 'genre',
			key: 'genre',
		},
		{
			title: 'Action',
			key: 'action',
			render: (text, record) => (
				<Space size="middle">
					<Button
						type="primary"
						onClick={() => {
							setInput(record);
							showModal();
						}}
					>
						Edit
					</Button>
					<Button danger>Delete</Button>
				</Space>
			),
		},
	];

	return (
		<>
			<Modal
				title="Edit Movie"
				visible={isModalVisible}
				onOk={handleOk}
				onCancel={handleCancel}
			>
				<Form {...layout}>
					<Form.Item label="Movie Title" name="title">
						<Input
							type="text"
							name="title"
							onChange={handleChange}
							value={input.title}
							defaultValue={input.title}
						/>
					</Form.Item>
					<Form.Item label="Movie Description" name="description">
						<Input
							type="text"
							name="description"
							onChange={handleChange}
							value={input.description}
							defaultValue={input.description}
						/>
					</Form.Item>
					<Form.Item label="Image URL" name="image_url">
						<Input
							type="text"
							name="image_url"
							onChange={handleChange}
							value={input.image_url}
							defaultValue={input.image_url}
						/>
					</Form.Item>
					<Form.Item label="Movie Rating" name="rating">
						<Input
							type="text"
							name="rating"
							onChange={handleChange}
							value={input.rating}
							defaultValue={input.rating}
						/>
					</Form.Item>
					<Form.Item label="Review" name="review">
						<Input
							type="text"
							name="review"
							onChange={handleChange}
							value={input.review}
							defaultValue={input.review}
						/>
					</Form.Item>
					<Form.Item label="Duration" name="duration">
						<Input
							type="text"
							name="duration"
							onChange={handleChange}
							value={input.duration}
							defaultValue={input.duration}
						/>
					</Form.Item>
					<Form.Item label="Genre" name="genre">
						<Input
							type="text"
							name="genre"
							onChange={handleChange}
							value={input.genre}
							defaultValue={input.genre}
						/>
					</Form.Item>
					<Form.Item label="Year" name="year">
						<Input
							type="text"
							name="year"
							onChange={handleChange}
							value={input.year}
							defaultValue={input.year}
						/>
					</Form.Item>

					<Form.Item {...tailLayout}>
						<Button type="primary" htmlType="submit" onClick={onSubmit}>
							Submit
						</Button>
					</Form.Item>
				</Form>
			</Modal>
			<Modal
				title="Tambah Movie"
				visible={isModalVisibleAdd}
				onOk={handleOkAdd}
				onCancel={handleCancelAdd}
			>
				<Form {...layout}>
					<Form.Item label="Movie Title" name="title">
						<Input
							type="text"
							name="title"
							onChange={handleChange}
							value={input.title}
						/>
					</Form.Item>
					<Form.Item label="Movie Description" name="description">
						<Input
							type="text"
							name="description"
							onChange={handleChange}
							value={input.description}
						/>
					</Form.Item>
					<Form.Item label="Image URL" name="image_url">
						<Input
							type="text"
							name="image_url"
							onChange={handleChange}
							value={input.image_url}
						/>
					</Form.Item>
					<Form.Item label="Movie Rating" name="rating">
						<Input
							type="text"
							name="rating"
							onChange={handleChange}
							value={input.rating}
						/>
					</Form.Item>
					<Form.Item label="Review" name="review">
						<Input
							type="text"
							name="review"
							onChange={handleChange}
							value={input.review}
						/>
					</Form.Item>
					<Form.Item label="Duration" name="duration">
						<Input
							type="text"
							name="duration"
							onChange={handleChange}
							value={input.duration}
						/>
					</Form.Item>
					<Form.Item label="Genre" name="genre">
						<Input
							type="text"
							name="genre"
							onChange={handleChange}
							value={input.genre}
						/>
					</Form.Item>
					<Form.Item label="Year" name="year">
						<Input
							type="text"
							name="year"
							onChange={handleChange}
							value={input.year}
						/>
					</Form.Item>

					<Form.Item {...tailLayout}>
						<Button type="primary" htmlType="submit" onClick={onSubmitAdd}>
							Submit
						</Button>
					</Form.Item>
				</Form>
			</Modal>
			<Button type="primary" onClick={() => showModalAdd()}>
				Tambah Movie
			</Button>
			<Table dataSource={data} columns={columns} />;
		</>
	);
}
