import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Home from './screens/Home';
import ListMovie from './screens/ListMovie';
import {
	PageHeader,
	Card,
	Menu,
	Layout,
	Breadcrumb,
	Modal,
	Input,
	Button,
} from 'antd';
import './App.css';
import axios from 'axios';
import { UserContext, UserProvider } from './UserContext';

const { Meta } = Card;
const { Header, Footer, Content } = Layout;

export default function App() {
	const [registerModal, setRegisterModal] = React.useState(false);
	const [loginModal, setLoginModal] = React.useState(false);
	const [user, setUser] = React.useContext(UserContext);

	const [input, setInput] = React.useState({
		name: '',
		email: '',
		password: '',
	});

	const showLoginModal = () => {
		setLoginModal(true);
	};

	const handleLoginOk = () => {
		setLoginModal(false);
	};

	const onLoginSubmit = () => {
		axios
			.post('https://backendexample.sanbersy.com/api/user-login', {
				email: input.email,
				password: input.password,
			})
			.then((res) => {
				var user = res.data.user;
				var token = res.data.token;
				var currentUser = { name: user.name, email: user.email, token };

				setUser(currentUser);
				localStorage.setItem('user', JSON.stringify(currentUser));
				setLoginModal(false);
				setRegisterModal(false);
			})
			.catch((err) => {
				alert(err);
			});
	};
	const handleLoginCancel = () => {
		setRegisterModal(false);
	};

	// register

	const showModal = () => {
		setRegisterModal(true);
	};

	const handleOk = () => {
		setRegisterModal(false);
	};

	const onSubmit = () => {
		axios
			.post('https://backendexample.sanbersy.com/api/register', {
				name: input.name,
				email: input.email,
				password: input.password,
			})
			.then((res) => {
				var user = res.data.user;
				var token = res.data.token;
				var currentUser = { name: user.name, email: user.email, token };

				setUser(currentUser);
				localStorage.setItem('user', JSON.stringify(currentUser));
				setLoginModal(false);
				setRegisterModal(false);
			})
			.catch((err) => {
				alert(err);
			});
	};
	const handleCancel = () => {
		setRegisterModal(false);
	};

	const handleChange = (event) => {
		let value = event.target.value;
		let name = event.target.name;
		switch (name) {
			case 'name': {
				setInput({ ...input, name: value });
				break;
			}
			case 'email': {
				setInput({ ...input, email: value });
				break;
			}
			case 'password': {
				setInput({ ...input, password: value });
				break;
			}
			default: {
				break;
			}
		}
	};
	return (
		<Router>
			<Layout className="layout">
				<Header>
					<div className="logo" />
					<Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
						<Menu.Item key="1">
							<Link to="/">Home</Link>
						</Menu.Item>
						<Menu.Item key="2">
							<Link to="/list-movie">List Movie</Link>
						</Menu.Item>
						{user ? (
							<Menu.Item
								key="3"
								onClick={() => {
									localStorage.clear();
								}}
							>
								Selamat Datang {user.name}
							</Menu.Item>
						) : (
							<>
								<Menu.Item
									key="3"
									onClick={() => {
										showLoginModal();
									}}
								>
									Login
								</Menu.Item>
								<Menu.Item
									key="4"
									onClick={() => {
										showModal();
									}}
								>
									Register
								</Menu.Item>
							</>
						)}
					</Menu>
				</Header>
				<Content style={{ padding: '0 50px' }}>
					<Modal
						title="Register"
						visible={registerModal}
						onOk={handleOk}
						onCancel={handleCancel}
					>
						<form onSubmit={onSubmit}>
							<label>name: </label>
							<Input
								type="text"
								name="name"
								onChange={handleChange}
								value={input.name}
							/>
							<br />
							<label>email: </label>
							<Input
								type="email"
								name="email"
								onChange={handleChange}
								value={input.email}
							/>
							<br />
							<label>Password: </label>
							<Input
								type="password"
								name="password"
								onChange={handleChange}
								value={input.password}
							/>
							<br />
							<Button onClick={onSubmit}>Register</Button>
						</form>
					</Modal>
					<Modal
						title="Login"
						visible={loginModal}
						onOk={handleLoginOk}
						onCancel={handleLoginCancel}
					>
						<form onSubmit={onLoginSubmit}>
							<label>email: </label>
							<Input
								type="email"
								name="email"
								onChange={handleChange}
								value={input.email}
							/>
							<br />
							<label>Password: </label>
							<Input
								type="password"
								name="password"
								onChange={handleChange}
								value={input.password}
							/>
							<br />
							<Button onClick={onLoginSubmit}>Login</Button>
						</form>
					</Modal>
					<div className="site-layout-content">
						<div>
							{/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
							<Switch>
								<Route path="/list-movie">
									<ListMovie />
								</Route>
								<Route path="/">
									<Home />
								</Route>
							</Switch>
						</div>
					</div>
				</Content>
				<Footer style={{ textAlign: 'center' }}>
					Created with love by Fadhur
				</Footer>
			</Layout>
		</Router>
	);
}
