function halo() {
  x="Halo Sanbers!";
  return x
  
}
console.log(halo()); // No 1

var num1 = 12
var num2 = 4

function kalikan(angka1, angka2) {
  return angka1 * angka2
}
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali)  // no 2

function introduce(a,b,c,d){
var x = [a,b,c,d];
var text = 'Nama saya '+ x[0] + ', umur saya ' + x[1] + ' tahun' + ', alamat saya di ' + x[2] + ' dan saya punya hobby yaitu ' + x[3];
return text
}

var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // no 3

var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]

var nb = {
	nama : arrayDaftarPeserta[0],
	jenisKelamin : arrayDaftarPeserta[1],
	hobi : arrayDaftarPeserta[2],
	tahunLahir : arrayDaftarPeserta [3],
}

console.log(nb) // no 4

  var data = [{
nama: 'strawberry' ,
warna: 'merah' ,
biji: false ,
  harga: 9000}, 
  {
nama: 'jeruk' ,
warna: 'oranye' ,
biji: true ,
  harga: 8000},
  {
nama: 'Semangka' ,
warna: 'Hijau & Merah' ,
biji: true ,
harga: 10000  	  
},
{
nama: 'Pisang' ,
warna: 'Kuning' ,
biji: false ,
harga: 5000
}]

var arraynama = data.map(function(item){
   return item.nama
})
var arraywarna = data.map(function(item){
   return item.warna
})
var arraybiji = data.map(function(item){
   return item.biji
})
var arrayharga = data.map(function(item){
   return item.harga
})


var arraybuahfilter1 = data.filter(function(item){
   return item.harga != 10000;
})
var arraybuahfilter2 = arraybuahfilter1.filter(function(item){
   return item.harga != 8000;
})
var arraybuahfilter3 = arraybuahfilter2.filter(function(item){
   return item.harga != 5000;
})
console.log(arraybuahfilter3) // No 5


var dataFilm = [];

let counter = 0;

function tambah(){
   var namaFilm = [
     {
        "nama":"Tukang Bubur Naik Haji",
        "durasi":"24 jam",
        "genre":"metal",
        "tahun":1998
     },
     {
        "nama":"Tukang Bubur Naik BMW",
        "durasi":"25 jam",
        "genre":"Pop Punk",
        "tahun":1999
     },
     {
        "nama":"Tukang Bubur Naik Roket",
        "durasi":"26 jam",
        "genre":"Indie",
        "tahun":2000
     },
     {
        "nama":"Tukang Bubur Naik Jabatan",
        "durasi":"27 jam",
        "genre":"Drama",
        "tahun":2010
     }
    ];
  
  dataFilm.push(namaFilm[counter])
  counter++;
}

tambah()
tambah()
tambah()
tambah()
console.log(dataFilm) // no 6
	