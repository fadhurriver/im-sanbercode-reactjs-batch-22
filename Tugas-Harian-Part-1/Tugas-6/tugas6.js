const luaslingkaran = (r, pi = 3.14) => {
  return r * r * pi;
};

const keliling = (r, pi = 3.14) => {
  return 2 * r * pi;
};

x = luaslingkaran(2);
z = keliling(4);
console.log(x);
console.log(z); // no1

let kalimat = "";

const k = ["saya", "adalah", "seorang", "frontend", "developer"];

let counter = 0;
function tambah() {
  kalimat += k[counter] + " ";
  counter++;
}

tambah();
tambah();
tambah();
tambah();
tambah();

console.log(kalimat); // no2

const newFunction = function literal(firstName, lastName) {
  console.log(`${firstName} ${lastName}`);
};

//Driver Code
newFunction("William", "Imoh"); // no3

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!",
};

const { firstName, lastName, destination, occupation, spell } = newObject;
console.log(firstName, lastName, destination, occupation, spell);

//no 4

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west, ...east]

console.log(combined) // no5