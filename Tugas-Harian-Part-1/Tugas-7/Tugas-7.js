class Animal {
    constructor(name) {
      this.name = name;
    }
    get nama() {
      return this.name;
    }
    set nama(x) {
      this.name = x;
    }
    get legs() {
      return 4;
    }
    set legs(z) {
      legs = z;
    }
    cold_blooded(c) {
      return cold_blooded;
    }
    get cold_blooded() {
      return false;
    }
    set cold_blooded(c) {
      cold_blooded = c;
    }
  }
  
  var sheep = new Animal("shaun");
  console.log(sheep.name);
  console.log(sheep.legs);
  console.log(sheep.cold_blooded);
  
  class Ape extends Animal {
    constructor(name) {
      super(name);
    }
    get legs() {
      return 2;
    }
    set legs(z) {
      legs = z;
    }
    yell() {
      return "Auooo";
    }
  }
  
  var sungokong = new Ape("kera sakti");
  console.log(sungokong.name);
  console.log(sungokong.legs);
  console.log(sungokong.cold_blooded);
  console.log(sungokong.yell());
  
  class Frog extends Animal {
    constructor(name) {
      super(name);
    }
    jump() {
      return "hop hop";
    }
  }
  
  var kodok = new Frog("buduk");
  console.log(kodok.name);
  console.log(kodok.legs);
  console.log(kodok.cold_blooded);
  console.log(kodok.jump());
  
  // No 1


  class Clock {
    constructor({ template }) {
      this.template = template;
    }
  
    render() {
      let date = new Date();
  
      let hours = date.getHours();
      if (hours < 10) hours = "0" + hours;
  
      let mins = date.getMinutes();
      if (mins < 10) mins = "0" + mins;
  
      let secs = date.getSeconds();
      if (secs < 10) secs = "0" + secs;
  
      let output = this.template
        .replace("h", hours)
        .replace("m", mins)
        .replace("s", secs);
  
      console.log(output);
    }
  
    start() {
      this.render();
      this.timer = setInterval(this.render(), 1000);
    }
  
    stop() {
      clearInterval(this.timer);
    }
  
    start() {
      this.render();
      this.timer = setInterval(() => this.render(), 1000);
    }
  }
  
  var clock = new Clock({ template: "h:m:s" });
  clock.start();
  